# Gancio plugin - Map of places

Tested on gancio 1.8.0

## Clone this repo to your Gancio

```bash
$ cd /opt/gancio/gancio_plugins
$ git clone https://framagit.org/sedum/gancio-plugin-map-of-places.git
```
