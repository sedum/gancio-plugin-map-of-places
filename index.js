const fs = require('fs')
const path = require('path')
const express = require('express')
const myPluginRouter = express.Router()

// check if plugin is located in /gancio_plugins or in /plugins
const PWD = __dirname.includes('/gancio_plugins') && 'gancio_plugins' || 'plugins'

// plugin settings
const settingsController = require('../../server/api/controller/settings')

// vue templates and marker green image
const pluginMapPage = fs.readFileSync("./"+PWD+"/gancio-plugin-map-of-places/Map.vue")
const pluginMapComponent = fs.readFileSync("./"+PWD+"/gancio-plugin-map-of-places/MapComponent.vue")
const pluginMarkerIcon = fs.readFileSync("./"+PWD+"/gancio-plugin-map-of-places/marker-icon-green.png")

const { Place, Event } = require('../../server/api/models/models')
const { Op } = require('sequelize')
const { DateTime, Settings } = require('luxon')
const zone = Settings.defaultZoneName = settingsController.settings["instance_timezone"]

function endOfDay (date) { return DateTime.fromJSDate(date, { zone }).endOf('day').toUnixInteger() }

// this will answer at http://localhost:13120/api/plugin/Map%20of%20places/settings
myPluginRouter.get('/settings', (req, res) => {
  const settings = settingsController.settings['plugin_Map\ of\ places']
  if (settings.gp_mop_admin_only && req.user && req.user.is_admin) { return reply(res) } 
  else if (!settings.gp_mop_admin_only) { return reply(res) }
  // else if (settings.gp_mop_user_only && req.user) { return reply(res) }
  // else if (!settings.gp_mop_admin_only && !settings.gp_mop_user_only) { return reply(res) }
  return res.json("Not found")
})

async function reply(res) {
  let now = Math.floor(Date.now() / 1000)
  const endOfToday = endOfDay(DateTime.fromSeconds(now).toJSDate())
  const settings = settingsController.settings['plugin_Map\ of\ places']

  const placesObj = settings.gp_mop_future_events 
    && await Place.findAll({
      include: [{ model: Event, as: 'events', 
        attributes: [ 'id', 'start_datetime', 'end_datetime' ],
        where: { end_datetime: { [Op.gt]: now } },
        required: false
      }],
      where: { latitude: { [Op.ne]: null }, longitude: { [Op.ne]: null } }
    })
    || await Place.findAll({ where: { latitude: { [Op.ne]: null }, longitude: { [Op.ne]: null } } })

  let places = JSON.parse(JSON.stringify(placesObj))

  settings.gp_mop_future_events && places.forEach((place, key) => {
    // Future events are events that start after the end of today
    // Today events are events where now is between the start and the end, or where the start is after now and before the end of today
    places[key].hasFutureEvents = place.events.some(e => e.start_datetime > endOfToday)
    places[key].hasTodayEvents = place.events.some(e => ((e.start_datetime < now && now < e.end_datetime) 
      || (now < e.start_datetime && e.start_datetime < endOfToday)) )
  })

  const response = { settings, places }
  return res.json(response)
}

const plugin = {
  routeAPI: myPluginRouter,
  configuration: {
    name: 'Map\ of\ places',
    author: 'sedum',
    url: 'https://framagit.org/sedum/gancio-plugin-map-of-places',
    description: 'Creates a page with the map of all places. The map will be visible at http://<instance.tld>/map. If you want this link to be accessible to everyone, add it to the footer from the \'theme\' tab of the administration interface.',
    settings: {
      gp_mop_admin_only: {
        type: 'CHECK',
        description: 'The map is visible only to admin users'
      },
      // Diabled because of "Visitors first. We do not want logged user to get more features than random visitor. We don’t want users to register, except to post events and even then you can post an anonymous event."
      // gp_mop_user_only: {
      //   type: 'CHECK',
      //   description: 'The map is visible to registered users'
      // },
      gp_mop_future_events: {
        type: 'CHECK',
        description: 'Places that will host events today or in the future are marked differently'
      },
      gp_mop_center: {
        type: 'TEXT',
        description: 'The default map center',
        hint: 'The default value is \"41.89,12.48\"'
      },
      gp_mop_zoom: {
        type: 'NUMBER',
        description: 'The default map zoom',
        hint: 'The default value is 5'
      },
    }    
  },
  gancio: null,
  settings: null,

  load (gancio, settings) {
    // add vue templates
    fs.writeFileSync("./pages/Map.vue", pluginMapPage)
    fs.writeFileSync("./components/MapComponent.vue", pluginMapComponent)
    fs.writeFileSync("./static/marker-icon-green.png", pluginMarkerIcon)
  },

  unload () {
    console.info('Unload this plugin')
    // remove vue templates
    fs.existsSync("./pages/Map.vue") && console.log('exists') && fs.unlinkSync("./pages/Map.vue")
    fs.existsSync("./pages/Map.vue") && fs.unlinkSync("./pages/Map.vue")
    fs.existsSync("./static/marker-icon-green.png") && fs.unlinkSync("./static/marker-icon-green.png")
  },

  onTest () {
    console.info('called on "TEST" button pressed in admin interface')
    // test the plugin for 15 minutes
    fs.writeFileSync("./pages/Map.vue", pluginMapPage)
    fs.writeFileSync("./components/MapComponent.vue", pluginMapComponent)
    fs.writeFileSync("./static/marker-icon-green.png", pluginMarkerIcon)
    setTimeout(() => {
      fs.unlinkSync("./pages/Map.vue")
      fs.unlinkSync("./components/MapComponent.vue")
      fs.unlinkSync("./static/marker-icon-green.png")
    }, 900000)
  }
}

module.exports = plugin

